# Data structure for the grid for the linear advection

### Copy out most of this code. Code commented with 3#s (like this) ###
### is here to help you to learn python and need not be copied      ###

### If you are using Python 2.7 rather than Python 3, import various###
### functions from Python 3 such as to use real number division     ###
### rather than integer division. ie 3/2  = 1.5  rather than 3/2 = 1###
from __future__ import absolute_import, division, print_function

### The numpy package for numerical functions and pi                ###
import numpy as np

### The class, or data-structure, Grid, stores all of the grid data ###
### so that all of the grid data can be passed to functions as one. ###
### The Grid function __init__ initialises the Grid class. From     ###
### within a class definition, "self" refers to the class itself    ###
class Grid(object):
    "Store all grid data and calculates dx and x locations."
    "The grid is assumed periodic."
    def __init__(self, nx, xmin=0.0, xmax=1.0):
        self.xmin = xmin
        self.xmax = xmax
        self.nx = nx
        self.length = self.xmax - self.xmin
        self.dx = self.length/self.nx
        # The x locations, excluding the end point
        self.x = np.arange(self.xmin, self.xmax, self.dx)

